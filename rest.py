# https://www.w3schools.com/python/module_requests.asp
# https://dresden-elektronik.github.io/deconz-rest-doc/getting_started/

import requests
from requests.auth import HTTPDigestAuth
import json


# TODO default args mit timeout !
# TODO generische func Arguments

class restMethods:

    @staticmethod
    def get_request_noCred(url, method, params, args):
        request = getattr(requests, method)
        myRequest = request(url, params=params, *args)

        if myRequest.ok:
            jsonData = json.loads(myRequest.content)
            return jsonData
        else:
            myRequest.raise_for_status()
            return {"error": myRequest.status_code}

    @staticmethod
    def get_request_wthCred(url, method, params, args, username, password):
        request = getattr(requests, method)
        myRequest = request(url, params=params, *args, auth=HTTPDigestAuth(username, password), verify=True)

        if myRequest.ok:
            jsonData = json.loads(myRequest.content)
            return jsonData
        else:
            myRequest.raise_for_status()
            return {"error": myRequest.status_code}

    @staticmethod
    def put_request_noCred(url, method, params, args):
        request = getattr(requests, method)
        myRequest = request(url, data=params, *args)

        if myRequest.ok:
            jsonData = json.loads(myRequest.content)
            return jsonData
        else:
            myRequest.raise_for_status()
            return {"error": myRequest.status_code}

    @staticmethod
    def put_request_wthCred(url, method, params, args, username, password):
        request = getattr(requests, method)
        myRequest = request(url, data=params, *args, auth=HTTPDigestAuth(username, password), verify=True)

        if myRequest.ok:
            jsonData = json.loads(myRequest.content)
            return jsonData
        else:
            myRequest.raise_for_status()
            return {"error": myRequest.status_code}

    @staticmethod
    def post_request_noCred(url, method, params, args):
        request = getattr(requests, method)
        myRequest = request(url, data=params, *args)

        if myRequest.ok:
            jsonData = json.loads(myRequest.content)
            return jsonData
        else:
            myRequest.raise_for_status()
            return {"error": myRequest.status_code}

    @staticmethod
    def post_request_wthCred(url, method, params, args, username, password):
        request = getattr(requests, method)
        myRequest = request(url, data=params, *args, auth=HTTPDigestAuth(username, password), verify=True)

        if myRequest.ok:
            jsonData = json.loads(myRequest.content)
            return jsonData
        else:
            myRequest.raise_for_status()
            return {"error": myRequest.status_code}
