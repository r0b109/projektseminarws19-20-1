# https://refactoring.guru/design-patterns/adapter
# https://realpython.com/python-dicts/
# translate between service(rest/mqtt) and opcua-methode

import json
from rest import restMethods


# from mqtt import mqttMethods


class restAdapter:
    def __init__(self, endpoint, method, args, auth, username, password):
        self.__endpoint = endpoint
        self.__method = method.lower()
        self.__args = args
        self.__auth = auth
        self.__username = username
        self.__password = password
        self.__jsonData = {}
        self.__paramsDict = {}

    def add_param(self, key, value):
        self.__paramsDict.update({key: value})

    def call_endpoint(self):
        self.__switch_method(self.__method)
        self.__paramsDict.clear()
    
    # TODO get_return JSON() -> uaMethod iteriert dann selber durch die keys
    
    def get_returnValue(self, key):
        return self.__jsonData.get(key)

    def __switch_method(self, method):
        switcher = {
            "get": self.__get,
            "put": self.__put,
            "post": self.__post
        }
        func = switcher.get(method, "Method not supported!")
        return func()

    def __get(self):
        if self.__auth is True:
            self.__jsonData = restMethods.get_request_wthCred(self.__endpoint, self.__method, self.__paramsDict,
                                                              self.__args, self.__username, self.__password)
        else:
            self.__jsonData = restMethods.get_request_noCred(self.__endpoint, self.__method, self.__paramsDict,
                                                             self.__args)

    def __put(self):
        if self.__auth is True:
            self.__jsonData = restMethods.put_request_wthCred(self.__endpoint, self.__method, self.__paramsDict,
                                                              self.__args, self.__username, self.__password)
        else:
            self.__jsonData = restMethods.put_request_noCred(self.__endpoint, self.__method, self.__paramsDict,
                                                             self.__args)

    def __post(self):
        if self.__auth is True:
            self.__jsonData = restMethods.post_request_wthCred(self.__endpoint, self.__method, self.__paramsDict,
                                                               self.__args, self.__username, self.__password)
        else:
            self.__jsonData = restMethods.post_request_noCred(self.__endpoint, self.__method, self.__paramsDict,
                                                              self.__args)


"""
    def __verify_JSON(self, jsonData):
        for key in jsonData:
            jsonData.pop(key)
        # iterate thr return ->if error: print message
"""


class mqttAdapter:
    pass
